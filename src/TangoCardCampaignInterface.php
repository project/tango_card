<?php

namespace Drupal\tango_card;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a Tango Card campaign entity.
 *
 * @ingroup tango_card_campaign
 */
interface TangoCardCampaignInterface extends ContentEntityInterface {

}
