<?php

namespace Drupal\tango_card;

/**
 * Wraps TangoCard SDK object with local configuration.
 */
interface TangoCardWrapperInterface {

  /**
   * Sets Tango Card object.
   *
   * @param \Sourcefuse\TangoCard $tango_card
   *   The Tango Card object to be wrapped.
   */
  public function setTangoCard(TangoCard $tango_card);

  /**
   * Gets Tango Card object, if set.
   *
   * @return \Sourcefuse\TangoCard
   *   The wrapped Tango Card object.
   */
  public function getTangoCard();

  /**
   * Sets account.
   *
   * @param \Drupal\tango_card\TangoCardAccountInterface $account
   *   Tango Card account entity.
   */
  public function setAccount(TangoCardAccountInterface $account);

  /**
   * Gets current account object, if set.
   *
   * @return \Drupal\tango_card\TangoCardAccountInterface|bool
   *   Tango Card account object, if exists. False otherwise.
   */
  public function getAccount();

  /**
   * Sets campaign.
   *
   * @param \Drupal\tango_card\TangoCardCampaignInterface $campaign
   *   Tango Card campaign entity.
   */
  public function setCampaign(TangoCardCampaignInterface $campaign);

  /**
   * Get current campaign object, if set.
   *
   * @return \Drupal\tango_card\TangoCardCampaignInterface|bool
   *   Tango Card campaign object, if exists. False otherwise.
   */
  public function getCampaign();

}
