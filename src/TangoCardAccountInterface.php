<?php

namespace Drupal\tango_card;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a Tango Card account entity.
 *
 * @ingroup tango_card_account
 */
interface TangoCardAccountInterface extends ContentEntityInterface {

}
